window.onload = function() {
    let image = document.querySelectorAll('.img');
    let nbImage = image.length;
    let count = 0;
    function imageCarousel() {
        setInterval(function(){
            image[count].classList.remove('active');
            if(count < nbImage -1){
                count++;
            } else {
                count = 0
            }
            image[count].classList.add('active')
        }, 3500)
    }

    const ratio = .5;
    const option = {
        root: null,
        rootMargin: '0px',
        threshold: ratio
    }
    const handleIntersect = function(entries, observer){
        entries.forEach(function (entry) {
            if (entry.intersectionRatio > ratio ) {
                entry.target.classList.add('reveal-visible')
                observer.unobserve(entry.target)
            }
        })
        console.log('handleIntersect');
    }
    const observer = new IntersectionObserver(handleIntersect, option);
    document.querySelectorAll('[class*="reveal-"]').forEach(function(r) {
        observer.observe(r)
    })

    
    
    let slide = document.querySelectorAll('.slide');
    let Prev = document.querySelector('#Prev')
    let Next = document.querySelector('#Next')
    let counter = 0;
    slide[counter].style.display = 'block';
    Prev.addEventListener('click', function() {
        console.log(counter)
        if (counter == 0) {
            slide[counter].style.display = 'none'
            counter = slide.length-1
            slide[counter].style.display = 'block'
            console.log(counter)
        } else {
            slide[counter].style.display = 'none'
            counter--
            slide[counter].style.display = 'block'

        }
    })
    Next.addEventListener('click', function(){
        if (counter == slide.length-1) {
            slide[counter].style.display = 'none'
            counter = 0
            slide[counter].style.display = 'block'
        } else {
            slide[counter].style.display = 'none'
            counter++
            slide[counter].style.display = 'block'

        }
    })

}


