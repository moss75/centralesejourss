<?php

namespace App\Form;

use App\Entity\Inscription;
use App\Entity\Sejours;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

class InscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Nom', TextType::class, [
                'label' => 'Nom de l\'enfant'
            ])
            ->add('Prenom')
            ->add('naissance', DateType::class, [
                'widget' => 'choice',
                'years' => range(date('1980'), date('2021')),

                
            ])
            ->add('taille')
            ->add('adresse')
            ->add('postal', IntegerType::class, [

            ])
            ->add('ville')
            ->add('numero', TextType::class, [
                'required' => true,
                'label' => 'Votre numero de telephone'
            ])
            ->add('mail', EmailType::class, [
                'label' => 'Votre Email',
                'constraints' => [
                    new Email([
                        'message' => 'Votre email est invalide'
                    ])
                ]
            ])
            ->add('sejours', EntityType::class, [
                'label' => 'Choix du sejour',
                'class' => Sejours::class,
                'choice_label' => 'titre'
            ])
            ->add('voyage', ChoiceType::class, [
                'label' => 'accompagnateur',
                'choices' => [
                    'oui'=> 'oui',
                    'non' => 'non'
                ]
            ])
            ->add('transport', ChoiceType::class, [
                'choices' => [
                    'depart de paris avec nous ' => 'depart de paris avec nous',
                    'depart a vos frais' => 'depart a vos frais'
                ]
            ])
            ->add('paiement', ChoiceType::class, [
                'choices' => [
                    'comptant' => 'comptant',
                    '3X sans frais' => '3X sans frais',
                    '4X sans frais' => '4X sans frais'
                ]
            ])
            ->add('accidentPersonne1', TextType::class, [
                'required' => true,
                'label' => ' Nom'
            ])
            ->add('accidentPersonne1Prenom', TextType::class, [
                'required' => true,
                'label' => ' Prenom'
            ])
            ->add('accidentPersonne1Telephone', TextType::class, [
                'required' => true,
                'label' => ' Numero'
            ])
            ->add('accidentPersonne2', TextType::class, [
                'required' => false,
                'label' => ' Nom'
            ])
            ->add('accidentPersonne2Prenom', TextType::class, [
                'required' => false,
                'label' => ' Prenom'
            ])
            ->add('accidentPersonne2Telephone', TextType::class, [
                'required' => false,
                'label' => ' Numero'
            ])
           ->add('Envoyer', SubmitType::class, [
               'attr' => ['class' => 'btn btn-dark mt-2']
           ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
             'data_class' => Inscription::class,
        ]);
    }
}
