<?php

namespace App\Form;

use App\Entity\Sejours;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SejoursFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Titre')
            ->add('Ville')
            ->add('Duree')
            ->add('Prix')
            ->add('Description')
            ->add('imageFile', VichImageType::class, [
                'required'=> false,
                'label' => 'Selectionner une photo',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez choisir une photo',
                        'groups' => ['new-picture']
                    ]),
                    new Image([
                        'maxSize' => '2G',
                        'maxSizeMessage' => 'Le fichier ne doit pas depasser les 2Mo',
                        'mimeTypes' => [
                            'image/gif', 
                            'image/png',
                            'image/jpeg',
                            'image/webp'
                        ],
                        'mimeTypesMessage' => 'Cette image est invalide, les formats acceptés sont : .png  .gif  .webp  .jepg'
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sejours::class,
        ]);
    }
}
