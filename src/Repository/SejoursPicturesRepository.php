<?php

namespace App\Repository;

use App\Entity\SejoursPictures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SejoursPictures|null find($id, $lockMode = null, $lockVersion = null)
 * @method SejoursPictures|null findOneBy(array $criteria, array $orderBy = null)
 * @method SejoursPictures[]    findAll()
 * @method SejoursPictures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SejoursPicturesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SejoursPictures::class);
    }

    // /**
    //  * @return SejoursPictures[] Returns an array of SejoursPictures objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SejoursPictures
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
