<?php

namespace App\Entity;

use App\Repository\SejoursPicturesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SejoursPicturesRepository::class)
 */
class SejoursPictures
{
    public function __toString()
    {
        return $this->name;
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Picture;

    /**
     * @ORM\ManyToOne(targetEntity=Sejours::class, inversedBy="sejoursPictures")
     */
    private $Sejours;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPicture(): ?string
    {
        return $this->Picture;
    }

    public function setPicture(string $Picture): self
    {
        $this->Picture = $Picture;

        return $this;
    }

    public function getSejours(): ?Sejours
    {
        return $this->Sejours;
    }

    public function setSejours(?Sejours $Sejours): self
    {
        $this->Sejours = $Sejours;

        return $this;
    }
}
