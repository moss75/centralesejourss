<?php

namespace App\Entity;

use App\Repository\InscriptionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InscriptionRepository::class)
 */
class Inscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenom;

    /**
     * @ORM\Column(type="date")
     */
    private $naissance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $taille;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $postal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\ManyToOne(targetEntity=Sejours::class, cascade={"persist", "remove"})
     */
    private $sejours;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $voyage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $transport;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Paiement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $AccidentPersonne1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $AccidentPersonne1Prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $AccidentPersonne1Telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $AccidentPersonne2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $AccidentPersonne2Telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $AccidentPersonne2Prenom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getNaissance(): ?\DateTimeInterface
    {
        return $this->naissance;
    }

    public function setNaissance(\DateTimeInterface $naissance): self
    {
        $this->naissance = $naissance;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getPostal(): ?string
    {
        return $this->postal;
    }

    public function setPostal(string $postal): self
    {
        $this->postal = $postal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getSejours(): ?Sejours
    {
        return $this->sejours;
    }

    public function setSejours(?Sejours $sejours): self
    {
        $this->sejours = $sejours;

        return $this;
    }

    public function getVoyage(): ?string
    {
        return $this->voyage;
    }

    public function setVoyage(string $voyage): self
    {
        $this->voyage = $voyage;

        return $this;
    }

    public function getTransport(): ?string
    {
        return $this->transport;
    }

    public function setTransport(string $transport): self
    {
        $this->transport = $transport;

        return $this;
    }

    public function getPaiement(): ?string
    {
        return $this->Paiement;
    }

    public function setPaiement(string $Paiement): self
    {
        $this->Paiement = $Paiement;

        return $this;
    }

    public function getAccidentPersonne1(): ?string
    {
        return $this->AccidentPersonne1;
    }

    public function setAccidentPersonne1(string $AccidentPersonne1): self
    {
        $this->AccidentPersonne1 = $AccidentPersonne1;

        return $this;
    }

    public function getAccidentPersonne1Prenom(): ?string
    {
        return $this->AccidentPersonne1Prenom;
    }

    public function setAccidentPersonne1Prenom(string $AccidentPersonne1Prenom): self
    {
        $this->AccidentPersonne1Prenom = $AccidentPersonne1Prenom;

        return $this;
    }

    public function getAccidentPersonne1Telephone(): ?string
    {
        return $this->AccidentPersonne1Telephone;
    }

    public function setAccidentPersonne1Telephone(string $AccidentPersonne1Telephone): self
    {
        $this->AccidentPersonne1Telephone = $AccidentPersonne1Telephone;

        return $this;
    }

    public function getAccidentPersonne2(): ?string
    {
        return $this->AccidentPersonne2;
    }

    public function setAccidentPersonne2(?string $AccidentPersonne2): self
    {
        $this->AccidentPersonne2 = $AccidentPersonne2;

        return $this;
    }

    public function getAccidentPersonne2Telephone(): ?string
    {
        return $this->AccidentPersonne2Telephone;
    }

    public function setAccidentPersonne2Telephone(?string $AccidentPersonne2Telephone): self
    {
        $this->AccidentPersonne2Telephone = $AccidentPersonne2Telephone;

        return $this;
    }

    public function getAccidentPersonne2Prenom(): ?string
    {
        return $this->AccidentPersonne2Prenom;
    }

    public function setAccidentPersonne2Prenom(?string $AccidentPersonne2Prenom): self
    {
        $this->AccidentPersonne2Prenom = $AccidentPersonne2Prenom;

        return $this;
    }
}
