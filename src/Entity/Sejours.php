<?php

namespace App\Entity;

use App\Repository\SejoursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SejoursRepository::class)
 */
class Sejours
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Titre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Duree;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Prix;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Image;

    /**
     * @ORM\OneToMany(targetEntity=SejoursPictures::class, mappedBy="Sejours")
     */
    private $sejoursPictures;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PrixTransport;

    public function __construct()
    {
        $this->sejoursPictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->Ville;
    }

    public function setVille(string $Ville): self
    {
        $this->Ville = $Ville;

        return $this;
    }

    public function getDuree(): ?string
    {
        return $this->Duree;
    }

    public function setDuree(string $Duree): self
    {
        $this->Duree = $Duree;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->Prix;
    }

    public function setPrix(string $Prix): self
    {
        $this->Prix = $Prix;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }
    public function __toString()
    {
        return $this->Titre;
    }


    /**
     * @return Collection|SejoursPictures[]
     */
    public function getSejoursPictures(): Collection
    {
        return $this->sejoursPictures;
    }

    public function addSejoursPicture(SejoursPictures $sejoursPicture): self
    {
        if (!$this->sejoursPictures->contains($sejoursPicture)) {
            $this->sejoursPictures[] = $sejoursPicture;
            $sejoursPicture->setSejours($this);
        }

        return $this;
    }

    public function removeSejoursPicture(SejoursPictures $sejoursPicture): self
    {
        if ($this->sejoursPictures->removeElement($sejoursPicture)) {
            // set the owning side to null (unless already changed)
            if ($sejoursPicture->getSejours() === $this) {
                $sejoursPicture->setSejours(null);
            }
        }

        return $this;
    }

    public function getPrixTransport(): ?string
    {
        return $this->PrixTransport;
    }

    public function setPrixTransport(string $PrixTransport): self
    {
        $this->PrixTransport = $PrixTransport;

        return $this;
    }
}
