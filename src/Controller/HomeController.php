<?php

namespace App\Controller;

use App\Entity\Caroussel;
use App\Entity\Sejours;
use App\Entity\SejoursPictures;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $caroussels = $this->getDoctrine()->getRepository(Caroussel::class)->findAll();
        $SejoursCaroussels = $this->getDoctrine()->getRepository(SejoursPictures::class)->findAll();
        $sejours = $this->getDoctrine()->getRepository(Sejours::class)->findOneBy([],['id' => 'DESC']);
        return $this->render('home/index.html.twig', [
            'sejours' => $sejours,
            'caroussels' => $caroussels,
            'SejoursCaroussels' => $SejoursCaroussels
        ]);
    }
    /**
     * @Route("/Projet", name="Projet")
     */
    public function projetEducatif(){
        return $this->render('home/ProjetEducatif.html.twig');
    }
}
