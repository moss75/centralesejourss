<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FooterController extends AbstractController
{
    /**
     * @Route("/MentionLegales", name="MentionLegales")
     */
    public function index(): Response
    {
        return $this->render('footer/index.html.twig', [
            'controller_name' => 'FooterController',
        ]);
    }
        /**
     * @Route("/Privacy", name="Privacy")
     */
    public function Privacy(): Response
    {
        return $this->render('footer/Privacy.html.twig', [
            'controller_name' => 'FooterController',
        ]);
    }
}
