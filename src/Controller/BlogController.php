<?php

namespace App\Controller;

use App\Entity\Blog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(): Response
    {
        $blog = $this->getDoctrine()->getRepository(Blog::class)->findAll();

        return $this->render('blog/index.html.twig', [
            'Blog' => $blog,
        ]);
    }
}
