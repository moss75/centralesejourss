<?php

namespace App\Controller;

use App\Entity\Inscription;
use App\Form\InscriptionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InscriptionController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription")
     */
    public function index(Request $request): Response
    {
        $inscription = new Inscription;
        $inscriptionForm = $this->createForm(InscriptionType::class, $inscription);
        $inscriptionForm->handleRequest($request);
        if ($inscriptionForm->isSubmitted() && $inscriptionForm->isValid()) {
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($inscription);
            $doctrine->flush();

            return $this->redirectToRoute('fiche_sanitaire');
            
        }
        return $this->render('inscription/index.html.twig', [
            'inscriptionForm' => $inscriptionForm->createView(),
        ]);
    }
}
