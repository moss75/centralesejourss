<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FicheSanitaireController extends AbstractController
{
    /**
     * @Route("/fiche/sanitaire", name="fiche_sanitaire")
     */
    public function index(): Response
    {
        return $this->render('fiche_sanitaire/index.html.twig', [
            'controller_name' => 'FicheSanitaireController',
        ]);
    }
}
