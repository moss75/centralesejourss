<?php

namespace App\Controller;

use App\Entity\Inscription;
use App\Entity\Sejours;
use App\Entity\SejoursPictures;
use App\Form\InscriptionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DecouvrirController extends AbstractController
{
    /**
     * @Route("/decouvrir/{id}", name="decouvrir_by_id")
     */
    public function index($id, Request $request): Response
    {
        $SejoursCaroussels = $this->getDoctrine()->getRepository(SejoursPictures::class)->findAll();
        $sejours = $this->getDoctrine()->getRepository(Sejours::class)->find($id);
        if(!$sejours) {
            throw $this->createNotFoundException('le sejours n\'existe pas');
        }
        $inscription = new Inscription;
        $inscriptionForm = $this->createForm(InscriptionType::class, $inscription);
        $inscriptionForm->handleRequest($request);
        if ($inscriptionForm->isSubmitted() && $inscriptionForm->isValid()) {
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($inscription);
            $doctrine->flush();

            return $this->redirectToRoute('fiche_sanitaire');
        }
        return $this->render('decouvrir/index.html.twig', [
            'sejours' => $sejours,
            'inscriptionForm' => $inscriptionForm->createView(),
            'SejoursCaroussels' => $SejoursCaroussels
        ]);
    }
}