<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use App\Entity\Caroussel;
use App\Entity\Sejours;
use App\Entity\SejoursPictures;
use App\Entity\User;
use App\Repository\BlogRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(SejoursCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Centrale Sejours Sport');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linktoRoute('Back to the website', 'fas fa-home', 'home');
        yield MenuItem::linkToCrud('Articles', 'fas fa-map-marker-alt', Sejours::class);
        yield MenuItem::linkToCrud('Caroussel', 'fas fa-map-marker-alt', Caroussel::class);
        yield MenuItem::linkToCrud('User', 'fas fa-map-marker-alt', User::class);
        yield MenuItem::linkToCrud('SejoursPictures', 'fas fa-map-marker-alt', SejoursPictures::class);
        yield MenuItem::linkToCrud('Blog', 'fas fa-map-marker-alt', Blog::class);
    }
}
