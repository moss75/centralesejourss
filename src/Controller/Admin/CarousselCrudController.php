<?php

namespace App\Controller\Admin;

use App\Entity\Caroussel;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class CarousselCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Caroussel::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ImageField::new('images', 'image')
            ->setBasePath('/images')
            ->setUploadDir('/public/images')
        ];
    }
}
