<?php

namespace App\Controller\Admin;

use App\Entity\Sejours;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SejoursCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Sejours::class;
    }

   
    public function configureFields(string $pageName): iterable
    {
        return [
            Textfield::new('titre'),
            TextField::new('ville'),
            TextField::new('duree'),
            TextEditorField::new('description'),
            TextField::new('prix'),

        ];
    }
    
}
