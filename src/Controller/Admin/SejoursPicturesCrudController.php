<?php

namespace App\Controller\Admin;

use App\Entity\SejoursPictures;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class SejoursPicturesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SejoursPictures::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('Sejours')
                ->autocomplete(),
            ImageField::new('picture', 'image')
                ->setBasePath('/images')
                ->setUploadDir('/public/images')
        ];
    }
    
}
