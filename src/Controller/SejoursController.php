<?php

namespace App\Controller;

use App\Entity\Sejours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SejoursController extends AbstractController
{
    /**
     * @Route("/sejours", name="sejours")
     */
    public function index(): Response
    {
        $sejours = $this->getDoctrine()->getRepository(Sejours::class)->findAll();
        return $this->render('sejours/index.html.twig', [
            'sejours' => $sejours,
            
        ]);
    }
}
